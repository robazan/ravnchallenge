//
//  UserTableViewCell.swift
//  RavnChallenge
//
//  Created by Rodrigo Bazan on 9/14/19.
//  Copyright © 2019 Rodrigo Bazan. All rights reserved.
//

import UIKit
import Nuke

class UserTableViewCell: UITableViewCell {

    @IBOutlet weak var userAvatar: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var loginLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func configure(userData: UserData){
        nameLabel.text = userData.login ?? ""
        loginLabel.text = userData.login ?? ""
        let url = URL(string: userData.avatar_url ?? "")!
        Nuke.loadImage(with: url, into: userAvatar)
    }
    
}
