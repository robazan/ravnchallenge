//
//  RequestToken.swift
//  RavnChallenge
//
//  Created by Rodrigo Bazan on 9/15/19.
//  Copyright © 2019 Rodrigo Bazan. All rights reserved.
//

import Foundation

class RequestToken {
    private weak var task: URLSessionDataTask?
    
    init(task: URLSessionDataTask) {
        self.task = task
    }
    
    func cancel() {
        task?.cancel()
    }
}
