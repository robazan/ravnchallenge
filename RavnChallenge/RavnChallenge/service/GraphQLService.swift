//
//  GraphQLService.swift
//  RavnChallenge
//
//  Created by Rodrigo Bazan on 9/14/19.
//  Copyright © 2019 Rodrigo Bazan. All rights reserved.
//

import UIKit

class GraphQLService: NSObject {

    //let token = "Bearer b9d8997679f2f2a04376dfcef9b74f7e46d790bd"
    let token = "Bearer PERSONAL_TOKEN"
    
    @discardableResult func getGitHugUsers(search: String, completion: @escaping (Result<GraphResponse, Error>) -> ()) -> RequestToken? {

        
        let url = "https://api.github.com/search/users?q=\(search)"
        
        let request = NSMutableURLRequest(url: NSURL(string: url)! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = ["Authorization": token]
        
        let session = URLSession.shared
        
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            guard let dataResponse = data, error == nil else {
                return
            }
            
            do {
                let parseResponse = try JSONDecoder().decode(GraphResponse.self, from: dataResponse)
                completion(.success(parseResponse))

            } catch let parsingError {
                completion(.failure(parsingError))
            }
        })
        
        dataTask.resume()
        return RequestToken(task: dataTask)
    }
    
}
