//
//  viewModel.swift
//  RavnChallenge
//
//  Created by Rodrigo Bazan on 9/14/19.
//  Copyright © 2019 Rodrigo Bazan. All rights reserved.
//

import UIKit

class viewModel: NSObject {

    let data = Models.shared
    private var requestToken: RequestToken?
    
    var reloadTv: (()->())?
    
    func searchText(text: String){
        self.requestToken?.cancel()
        
        let service = GraphQLService()
        
        self.requestToken = service.getGitHugUsers(search: text, completion: { (result) in
            switch result {
            case .success(let searchResult):
                self.data.data = searchResult
                self.reloadTv?()
            case .failure(let error):
                print(error)
                return
            }
        })
    }
    
    
    func getUserDataCell(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "UserTableViewCell", for: indexPath) as! UserTableViewCell
        cell.selectedBackgroundView?.backgroundColor = .clear
        cell.selectionStyle = .none
        if let userData = data.data?.items?[indexPath.row] {
            cell.configure(userData: userData)
        }
        return cell
    }

    
}


// MARK: - TableViewDelegate
extension viewModel: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 44.0
    }
    

    
}

extension viewModel: UITableViewDataSource {
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return nil
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.data?.items?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            return self.getUserDataCell(tableView, cellForRowAt: indexPath)
    }
}
