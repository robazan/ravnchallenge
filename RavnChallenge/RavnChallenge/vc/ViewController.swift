//
//  ViewController.swift
//  RavnChallenge
//
//  Created by Rodrigo Bazan on 9/14/19.
//  Copyright © 2019 Rodrigo Bazan. All rights reserved.
//

import UIKit
import RxSwift
import RxRelay
import RxCocoa



class ViewController: UIViewController {

    @IBOutlet weak var searchTxt: UITextField!
    @IBOutlet weak var tv: UITableView!

    private let disposeBag = DisposeBag()
    
    lazy var vm: viewModel = {
        viewModel()
    }()

    
    override func viewDidLoad() {
        super.viewDidLoad()

        //self.tv.register(UserTableViewCell.self, forCellReuseIdentifier: "UserTableViewCell")
        self.tv.register(UINib(nibName: "UserTableViewCell", bundle: nil), forCellReuseIdentifier: "UserTableViewCell")
        tv.delegate = vm
        tv.dataSource = vm
        self.configViewModel()
        
        let searchResult = searchTxt.rx.text.orEmpty
            .debounce(RxTimeInterval.milliseconds(300), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .flatMapLatest { (query) -> Observable<String> in
                if query.isEmpty {
                    return .just("")
                }
                
                return .just(query)
            }
            .observeOn(MainScheduler.instance)
        
        
        searchResult.asObservable()
            .subscribe(onNext: { txt in
                guard !txt.isEmpty else {
                    return
                }
                self.vm.searchText(text: txt)
            })
            .disposed(by: disposeBag)
        
        
    }

    fileprivate func configViewModel() {
        vm.reloadTv = {[weak self] in
            DispatchQueue.main.async {
                self?.tv.reloadData()
            }
        }
    }
}

